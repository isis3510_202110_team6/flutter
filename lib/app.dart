import 'package:clever_app/common/NavigationBar.dart';
import 'package:clever_app/common/Appbar.dart';
import 'package:clever_app/models/AnalyticsModel.dart';
import 'package:clever_app/screens/analytics.dart';
import 'package:clever_app/screens/cleverlynk.dart';
import 'package:clever_app/screens/orders.dart';
import 'package:clever_app/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:clever_app/models/OrderListModel.dart';
import 'package:clever_app/models/CleverlynkListModel.dart';
import 'models/AppModel.dart';

class App extends StatelessWidget {
  static List<Widget> _screens = <Widget>[
    Home(),
    ChangeNotifierProvider<OrderListLiteModel>(
        create: (_) => OrderListLiteModel(), child: Orders()),
    ChangeNotifierProvider<CleverlynkListModel>(
        create: (_) => CleverlynkListModel(), child: Cleverlynks()),
    ChangeNotifierProvider<AnalyticsModel>(
        create: (_) => AnalyticsModel(), child: Analytics()),
  ];

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AppModel>(
        create: (_) => AppModel(),
        builder: (context, child) =>
            Consumer<AppModel>(builder: (context, app, child) {
              return WillPopScope(
                  onWillPop: () async => app.removeScreen(),
                  child: Scaffold(
                    appBar: Appbar(context: context).renderAppBar(context),
                    bottomNavigationBar: NavigationBar(
                        onItemTapped: (val) {
                          app.index = val;
                          app.addHistory(val);
                        },
                        selectedIndex: app.index),
                    body: IndexedStack(
                      index: app.index,
                      children: <Widget>[..._screens],
                    ),
                  ));
            }));
  }
}
