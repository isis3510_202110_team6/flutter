import 'package:flutter/material.dart';

ThemeData lightTheme() {
  return ThemeData(
    brightness: Brightness.light,
    primaryColor: Color(0xFF02FCBB),
    backgroundColor: Colors.grey.shade50,
    cardColor: Colors.grey.shade100,
    errorColor: Colors.red.shade400,
    focusColor: Colors.tealAccent.shade400,
// Define the default font family.
    fontFamily: 'Inter',

// Define the default TextTheme. Use this to specify the default
// text styling for headlines, titles, bodies of text, and more.
    textTheme: TextTheme(
      headline1: TextStyle(
          fontSize: 24.0,
          fontWeight: FontWeight.bold,
          color: Colors.teal.shade900),
      headline2: TextStyle(
          color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
      headline3: TextStyle(
          color: Colors.black, fontSize: 14, fontWeight: FontWeight.normal),
      headline4: TextStyle(
          color: Colors.black, fontSize: 20, fontWeight: FontWeight.normal),
      headline5: TextStyle(
          color: Colors.black, fontSize: 12, fontWeight: FontWeight.normal),
      headline6: TextStyle(
          fontSize: 36.0, fontWeight: FontWeight.bold, fontFamily: 'Raleway'),
      bodyText1: TextStyle(
          color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold),
      bodyText2: TextStyle(
          color: Colors.blueGrey.shade900,
          fontSize: 12,
          fontWeight: FontWeight.normal),
      subtitle1: TextStyle(
          color: Colors.tealAccent.shade700,
          fontSize: 14,
          fontWeight: FontWeight.bold),
      subtitle2: TextStyle(
          color: Colors.black, fontSize: 16, fontWeight: FontWeight.normal),
      caption: TextStyle(
          color: Colors.black, fontSize: 22, fontWeight: FontWeight.normal),
    ),
  );
}

ThemeData darkTheme() {
  return ThemeData(
    focusColor: Colors.teal.shade500,
    cardColor: Colors.blueGrey.shade900,
    errorColor: Colors.red.shade400,
    brightness: Brightness.dark,
    backgroundColor: Colors.blueGrey.shade900,
    primaryColor: Color(0xFF02FCBB),
// Define the default font family.
    fontFamily: 'Inter',

// Define the default TextTheme. Use this to specify the default
// text styling for headlines, titles, bodies of text, and more.
    textTheme: TextTheme(
      headline1: TextStyle(
          fontSize: 24.0,
          fontWeight: FontWeight.bold,
          color: Colors.tealAccent.shade400),
      headline2: TextStyle(
          color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
      headline3: TextStyle(
          color: Colors.white, fontSize: 14, fontWeight: FontWeight.normal),
      headline4: TextStyle(
          color: Colors.tealAccent.shade400,
          fontSize: 20,
          fontWeight: FontWeight.normal),
      headline5: TextStyle(
          color: Colors.tealAccent.shade400,
          fontSize: 12,
          fontWeight: FontWeight.normal),
      headline6: TextStyle(
          fontSize: 36.0, fontWeight: FontWeight.bold, fontFamily: 'Raleway'),
      bodyText1: TextStyle(
          color: Colors.tealAccent.shade400,
          fontSize: 17,
          fontWeight: FontWeight.bold),
      bodyText2: TextStyle(
          color: Colors.tealAccent.shade200,
          fontSize: 12,
          fontWeight: FontWeight.normal),
      subtitle1: TextStyle(
          color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),
      subtitle2: TextStyle(
          color: Colors.white, fontSize: 16, fontWeight: FontWeight.normal),
      caption: TextStyle(
          color: Colors.tealAccent.shade200,
          fontSize: 22,
          fontWeight: FontWeight.normal),
    ),
  );
}
