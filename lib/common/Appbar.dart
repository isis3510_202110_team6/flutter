import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:clever_app/models/AuthenticationModel.dart';
import 'package:clever_app/screens/settingsScreen.dart';
import 'package:clever_app/screens/signIn.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class Appbar {
  var context;

  static const SIGNOUT = "SIGNOUT";
  static const SETTINGS = "SETTINGS";

  Appbar({this.context});
  void _signOut(BuildContext context) {
    try {
      var authenticationProvider =
          Provider.of<AuthenticationModel>(context, listen: false);
      authenticationProvider.signOut();
    } on AuthException catch (e) {
      print(e.message);
    }
  }

  AppBar renderAppBar(context) {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      iconTheme: Theme.of(context).iconTheme,
      actions: <Widget>[
        Builder(builder: (BuildContext context) {
          return PopupMenuButton(
              onSelected: (value) => {
                    if (value == SIGNOUT)
                      {
                        _signOut(context),
                        Navigator.of(context).pushReplacement(
                          MaterialPageRoute<void>(builder: (_) => SignIn()),
                        )
                      }
                    else if (value == SETTINGS)
                      {
                        Navigator.of(context).push(
                          MaterialPageRoute<void>(builder: (_) => Settings()),
                        )
                      }
                  },
              icon: Icon(
                Icons.settings_outlined,
                size: 30,
              ),
              padding: EdgeInsets.only(right: 20, top: 20),
              itemBuilder: (BuildContext context) {
                return [
                  PopupMenuItem(
                    value: SETTINGS,
                    child: Text(
                      'Configuración',
                    ),
                  ),
                  PopupMenuItem(
                    value: SIGNOUT,
                    child: Text(
                      'Cerrar Sesión',
                    ),
                  ),
                ];
              });
        })
      ],
    );
  }
}
