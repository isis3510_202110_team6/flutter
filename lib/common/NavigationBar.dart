import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NavigationBar extends StatelessWidget {
  final Function onItemTapped;
  final int selectedIndex;
  NavigationBar({this.onItemTapped, this.selectedIndex});
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          label: "Inicio",
          icon: Icon(Icons.home),
        ),
        BottomNavigationBarItem(
          label: "Ordenes",
          icon: Icon(Icons.shopping_bag),
        ),
        BottomNavigationBarItem(
          label: "Clynks",
          icon: Icon(Icons.arrow_forward),
        ),
        BottomNavigationBarItem(
          label: "Analítica",
          icon: Icon(Icons.analytics),
        ),
      ],
      currentIndex: selectedIndex,
      onTap: onItemTapped,
      unselectedItemColor: Colors.grey,
      selectedItemColor: Theme.of(context).primaryColor,
      showUnselectedLabels: true,
      iconSize: 30,
    );
  }
}
