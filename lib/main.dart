import 'package:clever_app/app.dart';
import 'package:clever_app/common/Themes.dart';
import 'package:clever_app/models/AuthenticationModel.dart';
import 'package:clever_app/models/OrderDetailModel.dart';
import 'package:clever_app/models/ThemeModel.dart';
import 'package:clever_app/screens/launchScreen.dart';
import 'package:clever_app/screens/signIn.dart';
import 'package:flutter/material.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:provider/provider.dart';
import 'amplifyconfiguration.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:io';
import 'package:get_it/get_it.dart';

import 'models/AppModel.dart';

GetIt getIt = GetIt.instance;
Future<void> main() async {
  getIt.registerSingleton<AppModel>(AppModel(), signalsReady: true);

  WidgetsFlutterBinding.ensureInitialized();
  await _configureAmplify();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<AuthenticationModel>(
              create: (_) => AuthenticationModel()),
          ChangeNotifierProvider<ThemeModel>(create: (_) => ThemeModel()),
          ChangeNotifierProvider<OrderDetailModel>(create: (_) => OrderDetailModel()),
        ],
        child: Consumer<ThemeModel>(builder: (context, ThemeModel theme, _) {
          var themeS = theme.internalTheme;
          ThemeMode themeM;
          if (themeS == ThemeModel.LIGHT) {
            themeM = ThemeMode.light;
          } else if (themeS == ThemeModel.DARK) {
            themeM = ThemeMode.dark;
          } else {
            themeM = ThemeMode.system;
          }
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'CleverApp',
            theme: lightTheme(),
            darkTheme: darkTheme(),
            themeMode: themeM,
            home: MyHomePage(),
          );
        }));
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ConnectivityResult previous;

  @override
  void dispose() {
    getIt<AppModel>().removeListener(update);
    super.dispose();
  }

  @override
  initState() {
    getIt
        .isReady<AppModel>()
        .then((_) => getIt<AppModel>().addListener(update));

    super.initState();
    try {
      InternetAddress.lookup('google.com').then((result) {
        if (!result.isNotEmpty && !result[0].rawAddress.isNotEmpty) {
          _showdialog();
        }
      }).catchError((error) {
        // no conn
        _showdialog();
      });
    } on SocketException catch (_) {
      // no internet
      _showdialog();
    }

    Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult connresult) {
      if (connresult == ConnectivityResult.none) {
        //_showdialog();
      } else if (previous == ConnectivityResult.none) {
        build(context);
      }

      previous = connresult;
    });
  }

  void update() => setState(() => {});

  void _showdialog() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text.rich(
          TextSpan(
            text: "¡Advertencia!",
            style: TextStyle(
                color: Colors.tealAccent.shade700,
                fontSize: 24,
                fontWeight: FontWeight.bold),
          ),
        ),
        content: Text(
            "No te encuentras conectado a internet, conéctate para ver tus "
            "ordenes o prender y apagar tus Clynks"),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, true),
            child: Text.rich(
              TextSpan(
                text: "OK Entiendo",
                style: TextStyle(
                    color: Colors.tealAccent.shade700,
                    fontSize: 14,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, AuthenticationModel auth, _) {
        switch (auth.status) {
          case Status.Authenticated:
            return App();
          case Status.Unauthenticated:
            return SignIn();
          case Status.Uninitialized:
            return LaunchScreen();
          default:
            return LaunchScreen();
        }
      },
    );
    // return _isSignedIn ? Home() : SignIn();
  }
}

Future _configureAmplify() async {
  // Add Pinpoint and Cognito Plugins, or any other plugins you want to use
  AmplifyAuthCognito authPlugin = AmplifyAuthCognito();
  Amplify.addPlugins([authPlugin]);

  // Once Plugins are added, configure Amplify
  // Note: Amplify can only be configured once.
  try {
    await Amplify.configure(amplifyconfig);
  } on AmplifyAlreadyConfiguredException {
    print(
        "Tried to reconfigure Amplify; this can occur when your app restarts on Android.");
  }
}
