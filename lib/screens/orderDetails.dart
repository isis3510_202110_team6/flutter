import 'package:clever_app/models/OrderDetailModel.dart';
import 'package:clever_app/models/OrderLiteModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:clever_app/models/AuthenticationModel.dart';
import 'package:connectivity/connectivity.dart';
import 'package:intl/intl.dart';
import 'package:cached_network_image/cached_network_image.dart';

class OrderDetails extends StatefulWidget {
  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  var connectionStatus;
  var order;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    order = ModalRoute.of(context).settings.arguments as OrderLiteModel;
    final id = order.id;
    var authenticationProvider =
        Provider.of<AuthenticationModel>(context, listen: false);
    var orderProvider = Provider.of<OrderDetailModel>(context, listen: false);
    try {
      orderProvider.fetchOrder(order.realId, authenticationProvider.jwt);
    } on Exception catch (e) {
      setState(() {
        connectionStatus = ConnectivityResult.none;
      });
      print(e);
    }

    List<Map<String, dynamic>> productos = [
      {
        "productName": "Huevos",
        "imagen": 'assets/images/Producto.png',
        "precio": 2000,
        "cantidad": 3,
      }
    ];
    return Consumer(
      builder: (context, OrderDetailModel orderDetailModel, _) {
        if (orderDetailModel.items.length == 0) {
          return Center(
              child: CircularProgressIndicator(
                  valueColor:
                      new AlwaysStoppedAnimation<Color>(Colors.teal.shade500)));
        }
        return SafeArea(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    alignment: Alignment.topLeft,
                    child: BackButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )),
                Container(
                  margin: const EdgeInsets.only(
                      left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                  width: double.infinity,
                  height: 50,
                  child: Center(
                    child: Text.rich(
                      TextSpan(
                        text: '''Orden: $id''',
                        style: Theme.of(context).textTheme.headline1,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 5.0),
                  child: ActionChip(
                    avatar: Icon(
                      Icons.moped,
                      color: Colors.black,
                      size: 16,
                    ),
                    label: Text(order.stringStatus),
                    backgroundColor: Colors.yellow.shade700,
                    elevation: 3.0,
                    onPressed: () {
                      print("Una función que cambie el estado de la orden");
                    },
                  ),
                ),
                Container(
                    margin: const EdgeInsets.only(left: 20.0, right: 20.0),
                    width: double.infinity,
                    height: 50,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            children: <Widget>[
                              Text.rich(
                                TextSpan(
                                  text: "Creado:",
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  text: _formatDate(order.createdAt),
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: <Widget>[
                              Text.rich(
                                TextSpan(
                                  text: "Entrega:",
                                  style: Theme.of(context).textTheme.subtitle1,
                                ),
                              ),
                              Text.rich(
                                TextSpan(
                                  text: order.deliveryDate,
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
                Container(
                  margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                  width: double.infinity,
                  child: Card(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: [
                                  Icon(
                                    Icons.mail,
                                    color: Colors.black,
                                    size: 16,
                                  ),
                                  Text.rich(
                                    TextSpan(
                                      text: orderDetailModel.identification,
                                      style:
                                          Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ),
                                ],
                              ),
                              ..._deliveryInfo(context)
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: List.generate(
                      orderDetailModel.items.length,
                      (index) => ProductCard(
                        name: orderDetailModel.items[index].name,
                        image: orderDetailModel.items[index].image,
                        price: orderDetailModel.items[index].price,
                        quantity: orderDetailModel.items[index].quantity,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                  width: double.infinity,
                  child: Card(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      text: "Valor",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                              Divider(
                                thickness: 2,
                              ),
                              Row(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      text: "Subtotal",
                                      style:
                                          Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ),
                                  Spacer(),
                                  Text.rich(
                                    TextSpan(
                                      text: "\$${orderDetailModel.subtotal}",
                                      style:
                                          Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      text: "Domicilio",
                                      style:
                                          Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ),
                                  Spacer(),
                                  Text.rich(
                                    TextSpan(
                                      text:
                                          "\$${orderDetailModel.deliveryPrice}",
                                      style:
                                          Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      text: "Propina",
                                      style:
                                          Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ),
                                  Spacer(),
                                  Text.rich(
                                    TextSpan(
                                      text: "\$${orderDetailModel.tip}",
                                      style:
                                          Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text.rich(
                                    TextSpan(
                                      text: "TOTAL",
                                      style:
                                          Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ),
                                  Spacer(),
                                  Text.rich(
                                    TextSpan(
                                      text: "\$${order.total}",
                                      style:
                                          Theme.of(context).textTheme.subtitle2,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  List<Widget> _deliveryInfo(BuildContext context) {
    var orderProvider = Provider.of<OrderDetailModel>(context, listen: false);
    List<Widget> deliveryInfoList = [];
    if (orderProvider.deliveryAddress != "") {
      deliveryInfoList.add(Row(
        children: [
          Icon(
            Icons.moped,
            color: Colors.black,
            size: 16,
          ),
          Text.rich(
            TextSpan(
              text: orderProvider.deliveryAddress,
              style: Theme.of(context).textTheme.subtitle2,
            ),
          ),
        ],
      ));
      if (orderProvider.deliveryAdditionalInfo != "") {
        deliveryInfoList.add(Row(
          children: [
            Icon(
              Icons.house,
              color: Colors.black,
              size: 16,
            ),
            Text.rich(
              TextSpan(
                text: orderProvider.deliveryAdditionalInfo,
                style: Theme.of(context).textTheme.subtitle2,
              ),
            ),
          ],
        ));
      }
    }
    return deliveryInfoList;
  }
}

class ProductCard extends StatelessWidget {
  const ProductCard({
    Key key,
    @required this.image,
    @required this.name,
    @required this.price,
    @required this.quantity,
  }) : super(key: key);

  final String name;
  final String image;
  final double price;
  final int quantity;

  @override
  Widget build(BuildContext context) {
    print(image);
    return Container(
      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
      width: double.infinity,
      height: 115,
      child: Card(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CachedNetworkImage(
                placeholder: (context, url) => image == ""
                    ? Icon(Icons.image)
                    : CircularProgressIndicator(),
                imageUrl: image,
                width: 70,
                height: 70,
                fit: BoxFit.cover,
              ),
              Container(
                margin: const EdgeInsets.only(left: 16.0, right: 16.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text.rich(
                        TextSpan(
                          text: name,
                          style: Theme.of(context).textTheme.headline2,
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          text: price.toString(),
                          style: Theme.of(context).textTheme.headline1,
                        ),
                      ),
                    ]),
              ),
              Spacer(),
              Text.rich(
                TextSpan(
                  text: 'x' + quantity.toString(),
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

String _formatDate(DateTime date) {
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final String formatted = formatter.format(date);
  return formatted;
}
