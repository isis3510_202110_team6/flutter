import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

class LaunchScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: Center(
        child: Theme.of(context).brightness == Brightness.light
            ? Image.asset('assets/images/cleverlynkLogoLightMode.png')
            : Image.asset('assets/images/cleverlynkLogoDarkMode.png'),
      ),
    );
  }
}
