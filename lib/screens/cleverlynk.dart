import 'package:flutter/material.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';
import 'package:provider/provider.dart';
import 'package:clever_app/models/CleverlynkListModel.dart';
import 'package:clever_app/models/AuthenticationModel.dart';
import 'package:connectivity/connectivity.dart';

class Cleverlynks extends StatefulWidget {
  @override
  _CleverlynksState createState() => _CleverlynksState();
}

class _CleverlynksState extends State<Cleverlynks> {
  @override
  void didChangeDependencies() {
    var authenticationProvider =
        Provider.of<AuthenticationModel>(context, listen: false);
    var cleverlynksProvider =
        Provider.of<CleverlynkListModel>(context, listen: false);
    try {
      if (cleverlynksProvider.cleverlynksList.length == 0) {
        cleverlynksProvider.fetchCleverlynks(
            authenticationProvider.username, authenticationProvider.jwt);
      }
    } on Exception catch (e) {
      setState(() {
        connectionStatus = ConnectivityResult.none;
      });
      print(e);
    } finally {
      super.didChangeDependencies();
    }
  }

  var connectionStatus;
  @override
  initState() {
    super.initState();
    initConnectivity();
  }

  void initConnectivity() async {
    var result = await Connectivity().checkConnectivity();
    setState(() {
      connectionStatus = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    initConnectivity();
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result != ConnectivityResult.none) {
        didChangeDependencies();
      }
      setState(() {
        connectionStatus = result;
      });
    });
    return Consumer(
        builder: (context, CleverlynkListModel cleverlynksModel, _) {
      return Stack(
        fit: StackFit.expand,
        children: [
          Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Center(
                  child: Text.rich(TextSpan(
                text: "Cleverlynks",
                style: Theme.of(context).textTheme.headline1,
              ))),
              Center(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: Text(
                    "Para ver más detalles de los cleverlynk ingresa a la página "
                    "web de Cleverlynk",
                    style: Theme.of(context).textTheme.bodyText1,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(
                    left: 20.0, right: 20.0, top: 5.0, bottom: 5.0),
                child: connectionStatus == ConnectivityResult.none
                    ? Center(
                        child: Text.rich(
                          TextSpan(
                            text:
                                "Conéctate a internet para poder apagar y prender tus Cleverlynks",
                            style: TextStyle(
                                color: Theme.of(context).errorColor,
                                fontSize: 12,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      )
                    : Text(""),
              ),
              Expanded(
                  child: Container(
                child: cleverlynksModel.cleverlynksList.isNotEmpty
                    ? ListView.builder(
                        itemCount: cleverlynksModel.cleverlynksList.length,
                        itemBuilder: (BuildContext context, int index) {
                          var cleverlynk =
                              cleverlynksModel.cleverlynksList[index];
                          return new Card(
                              margin: const EdgeInsets.only(
                                  left: 20.0,
                                  right: 20.0,
                                  top: 10.0,
                                  bottom: 10.0),
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              child: ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 15),
                                title: Text(
                                  cleverlynk.name,
                                  style: Theme.of(context).textTheme.headline2,
                                ),
                                subtitle: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        "Nuevas ordenes: ${cleverlynk.newOrdersAmount}"),
                                    Text(
                                        "Alcance: ${cleverlynk.amountRenders}"),
                                  ],
                                ),
                                trailing:
                                    connectionStatus == ConnectivityResult.none
                                        ? Text("")
                                        : Switch(
                                            value: cleverlynk.isActive,
                                            activeColor:
                                                Theme.of(context).focusColor,
                                            onChanged: (_) => _handleOnChange(
                                                cleverlynk.id,
                                                cleverlynk.isActive,
                                                context),
                                          ),
                              ));
                        })
                    : (cleverlynksModel.noClynks
                        ? noClynks()
                        : (connectionStatus == ConnectivityResult.none
                            ? noClynksCached()
                            : Center(
                                child: CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Colors.teal.shade500))))),
              ))
            ],
          ),
          //buildFloatingSearchBar(),
        ],
      );
    });
  }

  void _handleOnChange(
      String cleverlynkId, bool isActive, BuildContext context) {
    var authenticationProvider =
        Provider.of<AuthenticationModel>(context, listen: false);
    var cleverlynksProvider =
        Provider.of<CleverlynkListModel>(context, listen: false);
    var jwt = authenticationProvider.jwt;
    cleverlynksProvider.updateCleverlynkStatus(cleverlynkId, isActive, jwt);
  }

  Widget noClynks() {
    return Container(
      margin: const EdgeInsets.only(
          left: 40.0, right: 40.0, top: 50.0, bottom: 30.0),
      width: double.infinity,
      height: 400,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 320,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/NoCleverlynks.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Center(
            child: Text.rich(
              TextSpan(
                text:
                    "No has creado un Cleverlynk aún, entra a la página web para"
                    " crear tu primer Cleverlynk.",
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget noClynksCached() {
    return Container(
      margin: const EdgeInsets.only(
          left: 40.0, right: 40.0, top: 5.0, bottom: 20.0),
      width: double.infinity,
      height: 400,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 320,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/NoCleverlynkCached.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Center(
            child: Text.rich(
              TextSpan(
                text: "La última vez que tuviste acceso a internet no tenías "
                    "Cleverlynks creados... conéctate a internet para ver si ya "
                    "creaste uno.",
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget buildFloatingSearchBar() {
  return FloatingSearchBar(
    hint: 'Busca Cleverlynks',
    margins: EdgeInsets.only(top: 110, right: 15, left: 15),
    scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
    transitionDuration: const Duration(milliseconds: 800),
    transitionCurve: Curves.easeInOut,
    physics: const BouncingScrollPhysics(),
    openAxisAlignment: 0.0,
    debounceDelay: const Duration(milliseconds: 500),
    onQueryChanged: (query) {
      // Call your model, bloc, controller here.
    },
    // Specify a custom transition to be used for
    // animating between opened and closed stated.
    transition: CircularFloatingSearchBarTransition(),
    actions: [
      FloatingSearchBarAction.searchToClear(
        showIfClosed: false,
      ),
    ],
    builder: (context, transition) {
      return Container();
    },
  );
}
