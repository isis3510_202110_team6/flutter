import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:clever_app/models/CleverlynkListModel.dart';
import 'package:clever_app/models/CleverlynkLiteModel.dart';
import 'package:clever_app/models/AuthenticationModel.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_qrcode_scanner/flutter_qrcode_scanner.dart';

class CleverlynksQR extends StatefulWidget {
  const CleverlynksQR({
    Key key,
  }) : super(key: key);
  @override
  _CleverlynksState createState() => _CleverlynksState();
}

const flashOn = 'FLASH ON';
const flashOff = 'FLASH OFF';
const frontCamera = 'FRONT CAMERA';
const backCamera = 'BACK CAMERA';

class _CleverlynksState extends State<CleverlynksQR> {
  var qrText = '';
  Iterable<CleverlynkLiteModel> clynk;
  var openQR = true;
  var openDetailsCLYNK = true;
  var shortqrText = '';
  var flashState = flashOn;
  var cameraState = frontCamera;
  QRViewController controller;

  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  @override
  void didChangeDependencies() {
    var authenticationProvider =
        Provider.of<AuthenticationModel>(context, listen: true);
    var cleverlynksProvider =
        Provider.of<CleverlynkListModel>(context, listen: true);
    try {
      if (cleverlynksProvider.cleverlynksList.length == 0) {
        cleverlynksProvider.fetchCleverlynks(
            authenticationProvider.username, authenticationProvider.jwt);
      }
    } on HttpException catch (e) {
      setState(() {
        connectionStatus = ConnectivityResult.none;
      });
      print(e.message);
    } on SocketException catch (e) {
      setState(() {
        connectionStatus = ConnectivityResult.none;
      });
      print(e.message);
    } finally {
      super.didChangeDependencies();
    }
  }

  var connectionStatus;
  @override
  initState() {
    super.initState();
    initConnectivity();
  }

  void initConnectivity() async {
    var result = await Connectivity().checkConnectivity();
    setState(() {
      connectionStatus = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    initConnectivity();
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      setState(() {
        connectionStatus = result;
      });
    });
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Cleverlynks QR",
            style:
                TextStyle(color: Theme.of(context).textTheme.headline1.color),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          iconTheme: Theme.of(context).iconTheme,
        ),
        body: Consumer(
            builder: (context, CleverlynkListModel cleverlynksModel, _) {
          return Stack(
            fit: StackFit.expand,
            children: [
              Container(
                margin: const EdgeInsets.only(left: 20.0, right: 20.0),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: FittedBox(
                        fit: BoxFit.contain,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: connectionStatus == ConnectivityResult.none
                              ? <Widget>[
                                  Container(
                                    child: Column(children: [
                                      Text(
                                        "Ho hay conexión a internet \n"
                                        "conéctate para buscar por QR",
                                        style: TextStyle(
                                            color: Theme.of(context).errorColor,
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ]),
                                  ),
                                ]
                              : <Widget>[
                                  Container(
                                      child: openQR == true
                                          ? Text(
                                              "Cleverlynks QR",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline1,
                                            )
                                          : Text(
                                              clynk.first.name,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline1,
                                            )),
                                  Container(
                                      child: openQR == true
                                          ? Text(
                                              "Escanea el QR de tu Cleverlynk para ver su información",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .bodyText2,
                                              textAlign: TextAlign.center,
                                            )
                                          : Text("")),
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: openQR == true
                                          ? <Widget>[
                                              Container(
                                                child: IconButton(
                                                  icon: flashState == flashOn
                                                      ? Icon(Icons.flash_on)
                                                      : Icon(Icons.flash_off),
                                                  onPressed: () {
                                                    if (controller != null) {
                                                      controller.toggleFlash();
                                                      if (_isFlashOn(
                                                          flashState)) {
                                                        setState(() {
                                                          flashState = flashOff;
                                                        });
                                                      } else {
                                                        setState(() {
                                                          flashState = flashOn;
                                                        });
                                                      }
                                                    }
                                                  },
                                                ),
                                              ),
                                              Container(
                                                margin: EdgeInsets.all(8),
                                                child: IconButton(
                                                  icon: cameraState ==
                                                          frontCamera
                                                      ? Icon(Icons.camera_front)
                                                      : Icon(Icons
                                                          .photo_camera_back),
                                                  onPressed: () {
                                                    if (controller != null) {
                                                      controller.flipCamera();
                                                      if (_isBackCamera(
                                                          cameraState)) {
                                                        setState(() {
                                                          cameraState =
                                                              frontCamera;
                                                        });
                                                      } else {
                                                        setState(() {
                                                          cameraState =
                                                              backCamera;
                                                        });
                                                      }
                                                    }
                                                  },
                                                ),
                                              ),
                                              Container(
                                                child: IconButton(
                                                  icon: Icon(Icons.pause),
                                                  onPressed: () {
                                                    controller?.pauseCamera();
                                                  },
                                                ),
                                              ),
                                              Container(
                                                child: IconButton(
                                                  icon: Icon(Icons.play_arrow),
                                                  onPressed: () {
                                                    controller?.resumeCamera();
                                                  },
                                                ),
                                              ),
                                            ]
                                          : <Widget>[]),
                                  Container(
                                    child: Text('URL: $qrText'),
                                  ),
                                  Container(
                                      child: openQR == true
                                          ? ElevatedButton(
                                              style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateProperty
                                                        .all<Color>(Colors
                                                            .tealAccent
                                                            .shade700),
                                              ),
                                              onPressed: () {
                                                shortqrText = qrText.substring(
                                                    qrText.lastIndexOf('/') +
                                                        1);
                                                clynk = cleverlynksModel
                                                    .cleverlynksList
                                                    .where((theClynk) =>
                                                        theClynk.id.contains(
                                                            shortqrText));
                                                openQR = false;
                                                openDetailsCLYNK = true;
                                              },
                                              child: Text('Buscar CLYNK'),
                                            )
                                          : Text("")),
                                ],
                        ),
                      ),
                    ),
                    Expanded(
                        flex: 3,
                        child: connectionStatus == ConnectivityResult.none
                            ? Container(
                                width: double.infinity,
                                height: 320,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/NoCleverlynkCached.png'),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              )
                            : openQR == true
                                ? QRView(
                                    key: qrKey,
                                    onQRViewCreated: _onQRViewCreated,
                                    overlay: QrScannerOverlayShape(
                                      borderColor: Colors.red,
                                      borderRadius: 10,
                                      borderLength: 30,
                                      borderWidth: 10,
                                      cutOutSize: 300,
                                    ),
                                  )
                                : Container(
                                    child: Column(
                                      children: [
                                        Column(
                                          children: [
                                            Row(
                                              children: [
                                                Text("ESTADO",
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .caption),
                                                Switch(
                                                  value: clynk.first.isActive,
                                                  activeColor: Theme.of(context)
                                                      .focusColor,
                                                  onChanged: (_) =>
                                                      _handleOnChange(
                                                          clynk.first.id,
                                                          clynk.first.isActive,
                                                          context),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                    "Cantidad de renders: ${clynk.first.amountRenders}",
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .caption),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                    "Cantidad de nuevas ordenes: ${clynk.first.newOrdersAmount}",
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .caption),
                                              ],
                                            ),
                                          ],
                                        ),
                                        Container(
                                          margin: const EdgeInsets.only(
                                              left: 40.0,
                                              right: 40.0,
                                              top: 50.0,
                                              bottom: 30.0),
                                          child: ElevatedButton(
                                              style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateProperty
                                                        .all<Color>(Colors
                                                            .tealAccent
                                                            .shade700),
                                              ),
                                              onPressed: () {
                                                shortqrText = "";
                                                clynk = null;
                                                openQR = true;
                                                openDetailsCLYNK = false;
                                                qrText = "";
                                              },
                                              child: Text("Nueva Búsqueda")),
                                        ),
                                      ],
                                    ),
                                  )),
                  ],
                ),
              ),
            ],
          );
        }));
  }

  void _handleOnChange(
      String cleverlynkId, bool isActive, BuildContext context) {
    var authenticationProvider =
        Provider.of<AuthenticationModel>(context, listen: false);
    var cleverlynksProvider =
        Provider.of<CleverlynkListModel>(context, listen: false);
    var jwt = authenticationProvider.jwt;
    cleverlynksProvider.updateCleverlynkStatus(cleverlynkId, isActive, jwt);
  }

  bool _isFlashOn(String current) {
    return flashOn == current;
  }

  bool _isBackCamera(String current) {
    return backCamera == current;
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        qrText = scanData;
      });
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
