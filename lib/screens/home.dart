import 'package:clever_app/models/AppModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:clever_app/models/CleverlynkListModel.dart';
import 'package:clever_app/screens/cleverlynkQR.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var connectionStatus;
  @override
  initState() {
    super.initState();
    initConnectivity();
  }

  void initConnectivity() async {
    var result = await Connectivity().checkConnectivity();
    setState(() {
      connectionStatus = result;
    });
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      setState(() {
        connectionStatus = result;
      });
    });

    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(
                  left: 20.0, right: 20.0, top: 30.0, bottom: 30.0),
              width: double.infinity,
              height: 115,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: Theme.of(context).brightness == Brightness.light
                      ? AssetImage('assets/images/cleverlynkLogoLightMode.png')
                      : AssetImage('assets/images/cleverlynkLogoDarkMode.png'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Container(
                padding: const EdgeInsets.only(
                    left: 20.0, right: 20.0, top: 25.0, bottom: 10.0),
                margin: const EdgeInsets.only(
                    left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                width: double.infinity,
                height: 90,
                decoration: BoxDecoration(
                    image: DecorationImage(
                      image: Theme.of(context).brightness == Brightness.light
                          ? AssetImage(
                              'assets/images/HelloBackgroundLightMode.png')
                          : AssetImage(
                              'assets/images/HelloBackgroundDarkMode.png'),
                      fit: BoxFit.fill,
                    ),
                    color: Theme.of(context).backgroundColor,
                    borderRadius: BorderRadius.circular(20)),
                child: Text.rich(TextSpan(
                  text: "¡Bienvenido de nuevo!",
                  style: Theme.of(context).textTheme.headline2,
                  children: [
                    TextSpan(
                      text: "\n Cleverlynk",
                      style: Theme.of(context).textTheme.headline3,
                    ),
                  ],
                ))),
            Container(
                margin: const EdgeInsets.only(
                    left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                child: Expanded(
                    child: Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 10.0),
                      height: 130,
                      width: MediaQuery.of(context).size.width * 0.35,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            image: Theme.of(context).brightness ==
                                    Brightness.light
                                ? AssetImage('assets/images/dartLightMode.png')
                                : AssetImage('assets/images/dartDarkMode.png'),
                            fit: BoxFit.fill,
                          ),
                          color: Theme.of(context).backgroundColor,
                          borderRadius: BorderRadius.circular(20)),
                    ),
                    Container(
                        margin: const EdgeInsets.only(left: 10.0),
                        padding: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 20.0, bottom: 10.0),
                        width: MediaQuery.of(context).size.width * 0.48,
                        height: 130,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                              image: Theme.of(context).brightness ==
                                      Brightness.light
                                  ? AssetImage(
                                      'assets/images/ReminderBackgroundLightMode.png')
                                  : AssetImage(
                                      'assets/images/ReminderBackgroundDarkMode.png'),
                              fit: BoxFit.fill,
                            ),
                            color: Theme.of(context).backgroundColor,
                            borderRadius: BorderRadius.circular(20)),
                        child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute<void>(
                                    builder: (_) => ChangeNotifierProvider<
                                            CleverlynkListModel>(
                                        create: (_) => CleverlynkListModel(),
                                        child: CleverlynksQR())),
                              );
                            },
                            child: Text.rich(TextSpan(
                              text: "NUEVO!",
                              style: Theme.of(context).textTheme.headline4,
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nHAZ CLICK AQUI para buscar, apagar y prender tu CLYNK con su QR",
                                  style: Theme.of(context).textTheme.headline5,
                                ),
                              ],
                            )))),
                  ],
                ))),
            Container(
              padding: const EdgeInsets.only(
                  left: 100.0, right: 20.0, top: 25.0, bottom: 10.0),
              margin: const EdgeInsets.only(
                  left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
              width: double.infinity,
              height: 90,
              decoration: BoxDecoration(
                  image: DecorationImage(
                    image: Theme.of(context).brightness == Brightness.light
                        ? AssetImage('assets/images/HomeOrderLightMode.png')
                        : AssetImage('assets/images/HomeOrderDarkMode.png'),
                    fit: BoxFit.fill,
                  ),
                  color: Theme.of(context).backgroundColor,
                  borderRadius: BorderRadius.circular(20)),
              child: Consumer<AppModel>(builder: (context, app, child) {
                return GestureDetector(
                  child: Text.rich(TextSpan(
                    text: "¡Tienes ordenes nuevas!",
                    style: Theme.of(context).textTheme.bodyText1,
                    children: [
                      TextSpan(
                        text:
                            "\nHaz click aquí para revisar los nuevos pedidos de tus clientes",
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                    ],
                  )),
                  onTap: () {
                    app.index = 1;
                  },
                );
              }),
            ),
            Container(
                child: connectionStatus == ConnectivityResult.none
                    ? Container(
                        padding: const EdgeInsets.only(
                            left: 60.0, right: 20.0, top: 8.0, bottom: 8.0),
                        margin: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                        width: double.infinity,
                        height: 50,
                        child: Text.rich(
                          TextSpan(
                            text:
                                "¡ADVERTENCIA! NO ESTAS CONECTADO A INTERNET...",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        decoration: BoxDecoration(
                            color: Color(0XFFE53935),
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                                image: AssetImage(
                                    'assets/images/NoInternet.png'))),
                      )
                    : Text("")),
          ],
        ),
      ),
    );
  }
}
