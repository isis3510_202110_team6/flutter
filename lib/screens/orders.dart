import 'package:clever_app/models/AuthenticationModel.dart';
import 'package:clever_app/models/OrderLiteModel.dart';

import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';
import 'package:provider/provider.dart';
import 'package:clever_app/models/OrderListModel.dart';
import 'package:connectivity/connectivity.dart';
import 'package:clever_app/screens/orderDetails.dart';

class Orders extends StatefulWidget {
  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {
  var connectionStatus;
  @override
  initState() {
    super.initState();
    initConnectivity();
  }

  void initConnectivity() async {
    var result = await Connectivity().checkConnectivity();
    setState(() {
      connectionStatus = result;
    });
  }

  final end = _formatDate(new DateTime.now());
  final begin = _formatDate(new DateTime.now().subtract(new Duration(days: 6)));

  @override
  void didChangeDependencies() {
    var authenticationProvider =
        Provider.of<AuthenticationModel>(context, listen: false);
    var ordersProvider =
        Provider.of<OrderListLiteModel>(context, listen: false);
    try {
      if (ordersProvider.orders.length == 0 && ordersProvider.ordersLastWeek) {
        ordersProvider.fetchLastSevenDaysOrder(
            authenticationProvider.username, authenticationProvider.jwt);
      }
    } on Exception catch (e) {
      setState(() {
        connectionStatus = ConnectivityResult.none;
      });
      print(e);
    } finally {
      super.didChangeDependencies();
    }
  }

  @override
  Widget build(BuildContext context) {
    initConnectivity();
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result != ConnectivityResult.none) {
        didChangeDependencies();
      }
      setState(() {
        connectionStatus = result;
      });
    });
    return Consumer(
      builder: (context, OrderListLiteModel ordersModel, _) {
        return Stack(
          fit: StackFit.expand,
          children: [
            Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Center(
                    child: Text.rich(TextSpan(
                  text: "Ordenes",
                  style: Theme.of(context).textTheme.headline1,
                ))),
                Container(
                    child: connectionStatus == ConnectivityResult.none
                        ? Center(
                            child: Text.rich(
                              TextSpan(
                                text:
                                    "Conéctate a internet para refrescar las ordenes...",
                                style: TextStyle(
                                    color: Theme.of(context).errorColor,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          )
                        : Text("")),
                Container(
                  margin: const EdgeInsets.only(
                      left: 15.0, right: 15.0, top: 5.0, bottom: 5.0),
                  height: 40,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    children: <Widget>[
                      TextButton(
                          onPressed: () => ordersModel
                              .filterOrdersByStatus(OrderStatusFilters.ALL),
                          child: Text("Todas",
                              style: Theme.of(context).textTheme.bodyText1)),
                      TextButton(
                          onPressed: () => ordersModel.filterOrdersByStatus(
                              OrderStatusFilters.ACCEPTED),
                          child: Text("Aceptadas",
                              style: Theme.of(context).textTheme.bodyText1)),
                      TextButton(
                          onPressed: () => ordersModel
                              .filterOrdersByStatus(OrderStatusFilters.PENDING),
                          child: Text("Sin Revisar",
                              style: Theme.of(context).textTheme.bodyText1)),
                      TextButton(
                          onPressed: () => ordersModel.filterOrdersByStatus(
                              OrderStatusFilters.REJECTED),
                          child: Text("Rechazadas",
                              style: Theme.of(context).textTheme.bodyText1)),
                      TextButton(
                          onPressed: () => ordersModel.filterOrdersByStatus(
                              OrderStatusFilters.DELIVERED),
                          child: Text("Despachadas",
                              style: Theme.of(context).textTheme.bodyText1)),
                      TextButton(
                          onPressed: () => ordersModel.filterOrdersByStatus(
                              OrderStatusFilters.ARCHIVED),
                          child: Text("Archivadas",
                              style: Theme.of(context).textTheme.bodyText1)),
                    ],
                  ),
                ),
                Expanded(
                    child: ordersModel.ordersToShow.isNotEmpty
                        ? ListView(
                            children: ordersModel.ordersToShow
                                .map(
                                  (order) => Card(
                                      margin: const EdgeInsets.only(
                                          left: 20.0,
                                          right: 20.0,
                                          top: 5.0,
                                          bottom: 5.0),
                                      elevation: 5,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0)),
                                      child: ListTile(
                                        onTap: (){  _handleTap(context, order);},
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 20, vertical: 15),
                                        title: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text("ID: ${order.id}",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headline4),
                                            Text("Total: \$ ${order.total}",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .subtitle2),
                                          ],
                                        ),
                                        subtitle: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                "Creado: ${_formatDate(order.createdAt)}"),
                                            Text(
                                                "Entrega: ${order.deliveryDate}"),
                                          ],
                                        ),
                                        trailing: connectionStatus ==
                                                ConnectivityResult.none
                                            ? Text("")
                                            : IconButton(
                                                onPressed: () => _handleHidden(
                                                    order.id,
                                                    order.hidden,
                                                    context),
                                                icon: Icon(
                                                    Icons.archive_outlined)),
                                      )),
                                )
                                .toList(),
                          )
                        : (ordersModel.ordersLastWeek
                            ? (connectionStatus == ConnectivityResult.none
                                ? noCache()
                                : Center(
                                    child: CircularProgressIndicator(
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                                Theme.of(context).focusColor))))
                            : noOrdersLastWeek()))
              ],
            ),
            //buildFloatingSearchBar(),
          ],
        );
      },
    );
  }

  void _handleTap(BuildContext context, OrderLiteModel order) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Material(child: OrderDetails()),
        // Pass the arguments as part of the RouteSettings. The
        // DetailScreen reads the arguments from these settings.
        settings: RouteSettings(
          arguments: order,
        ),
      ),
    );
  }

  void _handleHidden(String orderId, bool hidden, BuildContext context) {
    var authenticationProvider =
        Provider.of<AuthenticationModel>(context, listen: false);
    var orderProvider = Provider.of<OrderListLiteModel>(context, listen: false);

    var jwt = authenticationProvider.jwt;
    orderProvider.updateOrderStatus(orderId, hidden, jwt);
  }

  Widget buildFloatingSearchBar() {
    return FloatingSearchBar(
      hint: 'Busca ordenes',
      margins: EdgeInsets.only(top: 50, right: 15, left: 15),
      scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
      transitionDuration: const Duration(milliseconds: 800),
      transitionCurve: Curves.easeInOut,
      physics: const BouncingScrollPhysics(),
      openAxisAlignment: 0.0,
      debounceDelay: const Duration(milliseconds: 500),
      onQueryChanged: (query) {
        // Call your model, bloc, controller here.
      },
      // Specify a custom transition to be used for
      // animating between opened and closed stated.
      transition: CircularFloatingSearchBarTransition(),
      actions: [
        FloatingSearchBarAction.searchToClear(
          showIfClosed: false,
        ),
      ],
      builder: (context, transition) {
        return Container();
      },
    );
  }

  Widget noCache() {
    return Container(
      margin: const EdgeInsets.only(
          left: 40.0, right: 40.0, top: 50.0, bottom: 30.0),
      width: double.infinity,
      height: 400,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 320,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/NoCache.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Center(
            child: Text.rich(
              TextSpan(
                  text: "La última vez que tuviste acceso a internet no tenías"
                      " ordenes recientes... conéctate a internet para ver"
                      " si hay nuevas ordenes.",
                  style: Theme.of(context).textTheme.headline5),
            ),
          ),
        ],
      ),
    );
  }

  Widget noOrdersLastWeek() {
    return Container(
      margin: const EdgeInsets.only(
          left: 60.0, right: 60.0, top: 50.0, bottom: 30.0),
      width: double.infinity,
      height: 400,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 320,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/NoOrdenesUltimaSemana.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Text.rich(
            TextSpan(
                text: "No has recibido ordenes en los últimos 7 días.",
                style: Theme.of(context).textTheme.subtitle1),
          ),
        ],
      ),
    );
  }
}

class Order {
  String id;
  DateTime created;
  DateTime delivery;
  String state;
  int price;

  Order(this.id, this.created, this.delivery, this.state, this.price);
}

String _formatDate(DateTime date) {
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final String formatted = formatter.format(date);
  return formatted;
}
