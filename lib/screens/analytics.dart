import 'package:clever_app/models/AnalyticsModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:clever_app/models/AuthenticationModel.dart';
import 'package:connectivity/connectivity.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class Analytics extends StatefulWidget {
  @override
  _AnalyticsState createState() => _AnalyticsState();
}

class _AnalyticsState extends State<Analytics> {
  @override
  void didChangeDependencies() {
    var authenticationProvider =
        Provider.of<AuthenticationModel>(context, listen: false);
    var analyticsProvider = Provider.of<AnalyticsModel>(context, listen: false);
    try {
      if (!analyticsProvider.updated) {
        analyticsProvider.fetchAnalytics(
            authenticationProvider.username, authenticationProvider.jwt);
        analyticsProvider.fetchTotalClynks(
            authenticationProvider.username, authenticationProvider.jwt);
      }
    } on Exception catch (e) {
      setState(() {
        connectionStatus = ConnectivityResult.none;
      });
      print(e);
    } finally {
      super.didChangeDependencies();
    }
  }

  var connectionStatus;
  @override
  initState() {
    super.initState();
    initConnectivity();
  }

  void initConnectivity() async {
    var result = await Connectivity().checkConnectivity();
    setState(() {
      connectionStatus = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    initConnectivity();
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result != ConnectivityResult.none) {
        didChangeDependencies();
      }
      setState(() {
        connectionStatus = result;
      });
    });
    return Consumer(builder: (context, AnalyticsModel analyticsModel, _) {
      return Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Center(
              child: Text.rich(TextSpan(
            text: "Analiticas",
            style: Theme.of(context).textTheme.headline1,
          ))),
          Container(
            margin: const EdgeInsets.only(
                left: 20.0, right: 20.0, top: 5.0, bottom: 5.0),
            child: connectionStatus == ConnectivityResult.none
                ? Center(
                    child: Text.rich(
                      TextSpan(
                        text:
                            "Conéctate a internet para poder ver tus analiticas",
                        style: TextStyle(
                            color: Theme.of(context).errorColor,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                : Text(""),
          ),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.only(top: 1),
            child: connectionStatus != ConnectivityResult.none
                ? ListView(children: [
                    Card(
                        margin: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                        child: ListTile(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 15),
                          title: Text(
                            "Tasa de conversión: "
                            "${analyticsModel.conversionRate * 100} %",
                            style: Theme.of(context).textTheme.headline2,
                          ),
                        )),
                    Card(
                        margin: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                        child: ListTile(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 15),
                          title: Text(
                            "Alcance Total: "
                            "${analyticsModel.totalRenders} renderizaciones",
                            style: Theme.of(context).textTheme.headline2,
                          ),
                        )),
                    Card(
                        margin: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                        child: ListTile(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 15),
                          title: Text(
                            "Compras Exitosas: "
                            "${analyticsModel.totalOrders} compras",
                            style: Theme.of(context).textTheme.headline2,
                          ),
                        )),
                    Card(
                        margin: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                        child: ListTile(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 15),
                          title: Text(
                            "Cleverlynks Activos: "
                            "${analyticsModel.totalClynks} clynks",
                            style: Theme.of(context).textTheme.headline2,
                          ),
                        )),
                    Card(
                        margin: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0)),
                        child: ConstrainedBox(
                            constraints: BoxConstraints(maxHeight: 250),
                            child: charts.TimeSeriesChart(
                              analyticsModel.seriesList,
                              animate: false,
                              // Optionally pass in a [DateTimeFactory] used by the chart. The factory
                              // should create the same type of [DateTime] as the data provided. If none
                              // specified, the default creates local date time.
                              dateTimeFactory:
                                  const charts.LocalDateTimeFactory(),
                              behaviors: [
                                new charts.SeriesLegend(
                                  showMeasures: true,
                                )
                              ],
                            ))),
                  ])
                : noClynks(),
          ))
        ],
      );
    });
  }

  Widget noClynks() {
    return Container(
      margin: const EdgeInsets.only(
          left: 40.0, right: 40.0, top: 50.0, bottom: 30.0),
      width: double.infinity,
      height: 400,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 320,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/NoCleverlynks.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Center(
            child: Text.rich(
              TextSpan(
                text:
                    "Las analiticas solo se pueden ver con una conexión a internet",
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
