import 'package:amplify_flutter/amplify.dart';
import 'package:flutter/material.dart';

/// Screen to recover the password of an existent user.
///
/// Has a basic form to input the email address and if ir is valid, it will send
/// a link to the provided email address to create a new one.
class RestorePassword extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RestorePasswordState();
}

class _RestorePasswordState extends State<RestorePassword> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _usernameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.greenAccent,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Form(
              key: _formKey,
              child: Center(
                  child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        child: const Text(
                          'Ingresa tu usuario y luego sigue el link que se te enviará',
                        ),
                        alignment: Alignment.center,
                      ),
                      TextFormField(
                        controller: _usernameController,
                        decoration: const InputDecoration(labelText: 'Usuario'),
                        validator: (String value) {
                          if (value.isEmpty) return 'Ingresa tu usuario';
                          return null;
                        },
                      ),
                      Container(
                        padding: const EdgeInsets.only(top: 16.0),
                        alignment: Alignment.center,
                        child: ElevatedButton(
                          child: Text("Enviar"),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              await Amplify.Auth.resetPassword(
                                  username: _usernameController.text.trim());
                              Navigator.of(context).pop();
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              )),
            ),
          ],
        ));
  }
}
