import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:clever_app/models/AuthenticationModel.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:clever_app/app.dart';
import 'package:connectivity/connectivity.dart';

//import 'package:clever_app/screens/restorePassword.dart';
//import 'package:clever_app/screens/register.dart';

/// The initial screen to Sign In a user.
///
/// Authenticates the user with email and password
class SignIn extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SignInState();
}

/// Manages the state for the [SignIn] widget.
class _SignInState extends State<SignIn> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final _storage = FlutterSecureStorage();
  final LocalAuthentication localAuth = LocalAuthentication();
  bool _canCheckBiometrics;
  bool _success;
  String _errorMessage;
  bool _wantsBiometric;
  var _wantsUpdate = false;
  var _hidePassword = true;
  bool _authenticating = false;
  var connectionStatus;
  @override
  initState() {
    super.initState();
    _checkBiometrics();
    _loadPreferences();
    initConnectivity();
  }

  void initConnectivity() async {
    connectionStatus = await Connectivity().checkConnectivity();
  }

  @override
  Widget build(BuildContext context) {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      setState(() => connectionStatus = result);
    });
    var heightScreen = MediaQuery.of(context).size.height;
    return Scaffold(
        body: Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: Theme.of(context).brightness == Brightness.light
                  ? AssetImage('assets/images/FondoClaro.png')
                  : AssetImage('assets/images/FondoOscuro.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: heightScreen * 0.15,
                child: Theme.of(context).brightness == Brightness.light
                    ? Image.asset('assets/images/cleverlynkLogoLightMode.png')
                    : Image.asset('assets/images/cleverlynkLogoDarkMode.png'),
              ),
              Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 30.0, right: 30.0, top: 5.0, bottom: 5.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      TextFormField(
                        controller: _usernameController,
                        decoration: const InputDecoration(
                            labelText: 'Usuario', focusColor: Colors.grey),
                        validator: (String value) {
                          if (value.isEmpty) return 'Ingresa tu usuario';
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: _passwordController,
                        decoration: InputDecoration(
                          labelText: 'Contraseña',
                          suffixIcon: IconButton(
                            icon: Icon(
                              _hidePassword
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                            ),
                            onPressed: () {
                              setState(() {
                                _hidePassword = !_hidePassword;
                              });
                            },
                          ),
                        ),
                        validator: (String value) {
                          if (value.isEmpty) return 'Ingresa tu contraseña';
                          return null;
                        },
                        obscureText: _hidePassword,
                      ),
                      // Shows a message to the user if there is an error trying to Sign In.
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        alignment: Alignment.center,
                        child: Text(
                          _success == null
                              ? ''
                              : (_success ? '' : _errorMessage ?? ''),
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.redAccent),
                        ),
                      ),
                      Container(
                          child: connectionStatus.toString() ==
                                  ConnectivityResult.none.toString()
                              ? Text.rich(
                                  TextSpan(
                                    text:
                                        "¡Conéctate a internet para iniciar sesión!",
                                    style: TextStyle(
                                        color: Colors.red.shade700,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                )
                              : Container(
                                  padding: const EdgeInsets.only(top: 16.0),
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: [
                                      ConstrainedBox(
                                        constraints: BoxConstraints.tightFor(
                                            width: double.infinity),
                                        child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                                primary: Theme.of(context)
                                                    .primaryColor, // background
                                                onPrimary:
                                                    Colors.black, // foreground
                                                shape:
                                                    new RoundedRectangleBorder(
                                                  borderRadius:
                                                      new BorderRadius.circular(
                                                          30.0),
                                                ),
                                                padding: EdgeInsets.all(16)),
                                            onPressed: () async {
                                              if (_formKey.currentState
                                                  .validate()) {
                                                _signInWithUsernameAndPassword(
                                                    _usernameController.text
                                                        .trim(),
                                                    _passwordController.text
                                                        .trim(),
                                                    context);
                                              }
                                            },
                                            child: Text("Iniciar Sesión")),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 10.0),
                                        child: _wantsBiometric != null
                                            ? (_wantsBiometric
                                                ? ConstrainedBox(
                                                    constraints:
                                                        BoxConstraints.tightFor(
                                                            width: double
                                                                .infinity),
                                                    child: ElevatedButton(
                                                      style: ElevatedButton
                                                          .styleFrom(
                                                              primary: Color(
                                                                  0xFF002F23), // background
                                                              onPrimary: Colors
                                                                  .white, // foreground
                                                              shape:
                                                                  new RoundedRectangleBorder(
                                                                borderRadius:
                                                                    new BorderRadius
                                                                            .circular(
                                                                        30.0),
                                                              ),
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(16)),
                                                      onPressed: () =>
                                                          _authenticate(
                                                              context),
                                                      child: Text(
                                                          "¡Ingresa con tus datos biométricos!"),
                                                    ),
                                                  )
                                                : Container())
                                            : Container(),
                                      ),
                                    ],
                                  ),
                                )),
                    ],
                  ),
                ),
              ),
            ],
            /*Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          children: [
                            Text(
                              '¿No tienes cuenta?',
                              style: TextStyle(
                                inherit: false,
                                color: Colors.black,
                                fontWeight: FontWeight.w300,
                                fontSize: 15,
                              ),
                            ),
                            TextButton(
                              onPressed: () => Navigator.of(context).push(
                                MaterialPageRoute<void>(
                                    builder: (_) => RegisterPage()),
                              ),
                              child: const Text(
                                'Crea una aquí',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                              '¿Olvidaste tu contraseña?',
                              style: TextStyle(
                                inherit: false,
                                color: Colors.black,
                                fontWeight: FontWeight.w300,
                                fontSize: 15,
                              ),
                            ),
                            TextButton(
                              onPressed: () => Navigator.of(context).push(
                                MaterialPageRoute<void>(
                                    builder: (_) => RestorePassword()),
                              ),
                              child: const Text(
                                'Has click aquí',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),*/
          ),
        ),
        _authenticating
            ? Center(
                child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(
                        Theme.of(context).focusColor)))
            : Container(),
      ],
    ));
  }

  // Clean up the controller when the Widget is disposed.
  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  /// Authenticates the user with the username and password provided and
  /// redirects to [App].
  ///
  /// If an error occurs, it changes the state of the main widget to show a message
  /// to the user.
  void _signInWithUsernameAndPassword(
      String username, String password, BuildContext bcontext) async {
    try {
      var authenticationProvider =
          Provider.of<AuthenticationModel>(bcontext, listen: false);

      setState(() {
        _authenticating = true;
      });
      var res = await authenticationProvider.signInWithUsernameAndPassword(
          username, password);

      setState(() {
        _authenticating = false;
      });

      if (res) {
        setState(() {
          _success = true;
        });
      } else {
        setState(() {
          _success = false;
          _errorMessage =
              "Verifica que tu usuario o contraseña estén correctos";
        });
      }

      if (_success) {
        if (_wantsBiometric == null && _canCheckBiometrics) {
          await showDialog(
              context: context,
              builder: (BuildContext context) =>
                  _buildPopupDialog(context, false));
          if (_wantsBiometric) {
            _storage.write(key: "username", value: username);
            _storage.write(key: "pswd", value: password);
          }
        } else if (_wantsBiometric) {
          final storedUsername = await _storage.read(key: "username");
          final storedPassword = await _storage.read(key: "pswd");
          if (username != storedUsername || password != storedPassword) {
            await showDialog(
                context: context,
                builder: (BuildContext context) =>
                    _buildPopupDialog(context, true));
            if (_wantsUpdate) {
              _storage.write(key: "username", value: username);
              _storage.write(key: "pswd", value: password);
            }
          }
        }
        Navigator.of(context).pushReplacement(
          MaterialPageRoute<void>(builder: (_) => App()),
        );
      }
    } on AuthException catch (e) {
      setState(() {
        _errorMessage = e.message;
        _success = false;
      });
    }
  }

  Widget _buildPopupDialog(BuildContext context, bool update) {
    return new AlertDialog(
      title: const Text('Autenticación biométrica'),
      content: Text(
          "¿Te gustaría guardar estos datos de inicio de sesión con tus datos "
          "biométricos para próximos intentos de inicio de sesión?"),
      actions: <Widget>[
        new TextButton(
          onPressed: () {
            if (update) {
              setState(() {
                _wantsUpdate = true;
              });
            } else {
              _updatePreferences(true);
            }
            Navigator.of(context).pop();
          },
          child: Text(
            'Sí',
            style: TextStyle(
              color: Theme.of(context).primaryColor,
            ),
          ),
        ),
        new TextButton(
          onPressed: () {
            if (!update) {
              _updatePreferences(false);
            }
            Navigator.of(context).pop();
          },
          child: Text(
            'No',
            style: TextStyle(
              color: Theme.of(context).primaryColor,
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await localAuth.canCheckBiometrics;
    } on PlatformException catch (e) {
      canCheckBiometrics = false;
      print(e);
    }
    if (!mounted) return;

    setState(() => _canCheckBiometrics = canCheckBiometrics);
  }

  _authenticate(BuildContext context) async {
    bool authenticated = false;
    try {
      authenticated = await localAuth.authenticate(
          localizedReason: 'Autentícate con tu método de preferencia',
          useErrorDialogs: true,
          stickyAuth: false);
    } on PlatformException catch (e) {
      print(e);
      return;
    }
    if (!mounted) return;

    if (authenticated) {
      final username = await _storage.read(key: "username");
      final password = await _storage.read(key: "pswd");
      _signInWithUsernameAndPassword(username, password, context);
    }
  }

  _loadPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _wantsBiometric = prefs.getBool('wants_biometric');
    });
  }

  _updatePreferences(bool pref) {
    SharedPreferences.getInstance()
        .then((prefs) => prefs.setBool('wants_biometric', pref));
    setState(() => _wantsBiometric = pref);
  }
}
