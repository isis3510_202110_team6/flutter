import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:flutter/material.dart';
import 'package:clever_app/app.dart';
import 'package:clever_app/screens/signIn.dart';

/// Screen to register a new user.
///
/// Creates a new user with email and password using the Firebase Auth service,
/// it also can redirect to [SignIn] if the user already has an account.
class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();

  bool _success;
  bool _check = true;
  String _errorMessage;
  var _hidePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.greenAccent,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Form(
              key: _formKey,
              child: Center(
                child: Card(
                  child: Padding(
                    padding: EdgeInsets.all(30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          child: const Text(
                            'Crea una nueva cuenta con tu usuario y contraseña',
                          ),
                          alignment: Alignment.center,
                        ),
                        TextFormField(
                          controller: _usernameController,
                          decoration:
                              const InputDecoration(labelText: 'Usuario'),
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Ingresa tu usuario';
                            }
                            return null;
                          },
                          keyboardType: TextInputType.name,
                        ),
                        TextFormField(
                          controller: _emailController,
                          decoration:
                              const InputDecoration(labelText: 'Correo'),
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Ingresa tu correo';
                            }
                            return null;
                          },
                          keyboardType: TextInputType.emailAddress,
                        ),
                        TextFormField(
                          controller: _passwordController,
                          decoration: InputDecoration(
                            labelText: 'Contraseña',
                            suffixIcon: IconButton(
                              icon: Icon(
                                _hidePassword
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                              ),
                              onPressed: () {
                                setState(() {
                                  _hidePassword = !_hidePassword;
                                });
                              },
                            ),
                          ),
                          validator: (String value) {
                            if (value.isEmpty) return 'Ingresa tu contraseña';
                            return null;
                          },
                          obscureText: _hidePassword,
                        ),
                        // Row(
                        //   children: [
                        //     Checkbox(
                        //         value: _check,
                        //         onChanged: (bool val) {
                        //           setState(() {
                        //             _check = val;
                        //           });
                        //         }),
                        //     Text("He leído los terminos y condiciones")
                        //   ],
                        // ),
                        Container(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          alignment: Alignment.center,
                          child: ElevatedButton(
                            onPressed: () async {
                              if (_formKey.currentState.validate() && _check) {
                                _register();
                              }
                            },
                            child: Text('Registrarse'),
                          ),
                        ),
                        // Shows a message to the user if there is an error trying to Register.
                        Container(
                          alignment: Alignment.center,
                          child: Text(
                            _success == null
                                ? ''
                                : (_success ? '' : _errorMessage),
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Theme.of(context).errorColor),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
          Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              children: [
                Text(
                  '¿Ya tienes cuenta?',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text(
                    'Inicia Sesión aquí',
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  // Clean up the controller when the Widget is disposed.
  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _usernameController.dispose();
    super.dispose();
  }

  /// Creates the new user with the email and password provided and redirects to [App].
  ///
  /// If an error occurs, it changes the state of the main widget to show a message
  /// to the user.
  void _register() async {
    try {
      Map<String, String> userAttributes = {
        'email': _emailController.text.trim()
      };

      await Amplify.Auth.signUp(
          username: _usernameController.text.trim(),
          password: _passwordController.text.trim(),
          options: CognitoSignUpOptions(userAttributes: userAttributes));

      setState(() {
        _success = true;
      });
      Navigator.of(context).push(
        MaterialPageRoute<void>(builder: (_) => App()),
      );
    } on AuthException catch (e) {
      setState(() {
        _errorMessage = e.message;
        _success = false;
      });
    }
  }
}
