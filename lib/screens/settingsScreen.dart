import 'package:clever_app/models/ThemeModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Configuración",
          style: TextStyle(color: Theme.of(context).textTheme.headline1.color),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        iconTheme: Theme.of(context).iconTheme,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Text(
              "Tema",
              style: Theme.of(context).textTheme.headline1,
            ),
          ),
          Divider(
            height: 5,
            thickness: 1,
            indent: 10,
            endIndent: 10,
          ),
          Consumer(builder: (context, ThemeModel theme, _) {
            return Column(
              children: [
                RadioListTile(
                  title: Text("Tema claro"),
                  subtitle: Text("Siempre la app tendrá el tema claro"),
                  value: ThemeModel.LIGHT,
                  groupValue: theme.theme,
                  onChanged: (value) => theme.theme = value,
                ),
                RadioListTile(
                  title: Text("Tema oscuro"),
                  subtitle: Text("Siempre la app tendrá el tema oscuro"),
                  value: ThemeModel.DARK,
                  groupValue: theme.theme,
                  onChanged: (value) => theme.theme = value,
                ),
                RadioListTile(
                  title: Text("Tema del sistema"),
                  subtitle: Text("La app tendrá el mismo tema que el sistema"),
                  value: ThemeModel.SYSTEM,
                  groupValue: theme.theme,
                  onChanged: (value) => theme.theme = value,
                ),
                RadioListTile(
                  title: Text("Tema automático"),
                  subtitle: Text("La app cambiará a tema oscuro a la hora del "
                      "atardecer según tu ubicación (datos de atardecer tomados"
                      " de sunrise-sunset.org)"),
                  value: ThemeModel.AUTO,
                  groupValue: theme.theme,
                  onChanged: (value) => theme.theme = value,
                ),
              ],
            );
          })
        ],
      ),
    );
  }
}
