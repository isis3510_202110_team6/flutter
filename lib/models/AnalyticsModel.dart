import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:charts_flutter/flutter.dart' as charts;

class AnalyticsModel extends ChangeNotifier {
  double conversionRate = 0.0499;
  int totalOrders = 805;
  int totalRenders = 16102;
  int totalClynks = 4;
  bool updated = true;
  List<charts.Series> seriesList;

  AnalyticsModel() {
    seriesList = _createSampleData();
  }

  void fetchAnalytics(String username, String jwt) async {
    String graphQLDocument = '''
     query q2{
      getConversionRateCleverlynks(filterCompanies:[]){
        conversionRate
        totalOrders
        totalRenders
      }
     }
    ''';
    http.Response response;
    var body = {
      'OperationName': 'getConversionRateCleverlynks',
      'query': graphQLDocument
    };

    try {
      http
          .post(
        Uri.parse(
            'https://igojikuei5b4hn2bfyremk7yde.appsync-api.us-east-1.amazonaws.com/graphql'),
        headers: {
          'Authorization': jwt,
          'Content-Type': 'application/json',
        },
        body: json.encode(body),
      )
          .then((value) {
        if (value.statusCode != 200) {
          throw HttpException("Hubo un error con la petición");
        }
        response = value;
        var res =
            jsonDecode(response.body)['data']['getConversionRateCleverlynks'];
        conversionRate = res['conversionRate'];
        totalOrders = res['totalOrders'];
        totalRenders = res['totalRenders'];
      });
    } catch (e) {
      print(e);
    }
  }

  void fetchTotalClynks(String username, String jwt) async {
    String graphQLDocument = '''
     query q3{
      cleverlynksByCompany(filterCompanies:[]){
        max
      }
     }
    ''';
    http.Response response;
    var body = {
      'OperationName': 'cleverlynksByCompany',
      'query': graphQLDocument
    };

    try {
      http
          .post(
        Uri.parse(
            'https://igojikuei5b4hn2bfyremk7yde.appsync-api.us-east-1.amazonaws.com/graphql'),
        headers: {
          'Authorization': jwt,
          'Content-Type': 'application/json',
        },
        body: json.encode(body),
      )
          .then((value) {
        if (value.statusCode != 200) {
          throw HttpException("Hubo un error con la petición");
        }
        response = value;
        var res = jsonDecode(response.body)['data']['cleverlynksByCompany'];
        totalClynks = res['max'];
      });
    } catch (e) {
      print(e);
    }
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<TimeSeriesSales, DateTime>> _createSampleData() {
    final data1 = [
      new TimeSeriesSales(new DateTime(2021, 6, 1), 5),
      new TimeSeriesSales(new DateTime(2021, 6, 2), 25),
      new TimeSeriesSales(new DateTime(2021, 6, 3), 100),
      new TimeSeriesSales(new DateTime(2021, 6, 4), 75),
      new TimeSeriesSales(new DateTime(2021, 6, 5), 85),
      new TimeSeriesSales(new DateTime(2021, 6, 6), 95),
      new TimeSeriesSales(new DateTime(2021, 6, 7), 65),
    ];

    final data2 = [
      new TimeSeriesSales(new DateTime(2021, 6, 1), 15),
      new TimeSeriesSales(new DateTime(2021, 6, 2), 5),
      new TimeSeriesSales(new DateTime(2021, 6, 3), 90),
      new TimeSeriesSales(new DateTime(2021, 6, 4), 80),
      new TimeSeriesSales(new DateTime(2021, 6, 5), 5),
      new TimeSeriesSales(new DateTime(2021, 6, 6), 65),
      new TimeSeriesSales(new DateTime(2021, 6, 7), 95),
    ];

    return [
      new charts.Series<TimeSeriesSales, DateTime>(
        id: 'Ventas semana actual',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (TimeSeriesSales sales, _) => sales.time,
        measureFn: (TimeSeriesSales sales, _) => sales.sales,
        data: data1,
      ),
      new charts.Series<TimeSeriesSales, DateTime>(
        id: 'Ventas semana anterior',
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        domainFn: (TimeSeriesSales sales, _) => sales.time,
        measureFn: (TimeSeriesSales sales, _) => sales.sales,
        data: data2,
      )
    ];
  }
}

/// Sample time series data type.
class TimeSeriesSales {
  final DateTime time;
  final int sales;

  TimeSeriesSales(this.time, this.sales);
}