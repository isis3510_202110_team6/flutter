import 'package:clever_app/models/OrderDetailModel.dart';
import 'package:clever_app/models/OrderLiteModel.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:intl/intl.dart';

final acceptedOrderCodes = [
  1,
  101,
  201,
  301,
  401,
  601,
  701,
  801,
  901,
  1001,
  1101,
  1201
];

final rejectedOrderCodes = [
  102,
  202,
  302,
  402,
  403,
  602,
  902,
  802,
  1002,
  1102,
  11,
  13,
  113,
  213,
  313,
  413,
  513,
  613,
  713,
  813,
  911,
  1013,
  1213
];

final pendingOrderCodes = [203, 303];

enum OrderStatusFilters {
  ALL,
  ACCEPTED,
  REJECTED,
  DELIVERED,
  PENDING,
  ARCHIVED
}

class OrderListLiteModel extends ChangeNotifier {
  List<OrderLiteModel> orders = [];
  List<OrderLiteModel> ordersToShow = [];
  bool ordersLastWeek = true;
  OrderDetailModel currentOrder;
  OrderStatusFilters currentFilter = OrderStatusFilters.ALL;
  final end = _formatDate(new DateTime.now());
  final begin = new DateTime.now().subtract(new Duration(days: 6));

  void fetchLastSevenDaysOrder(String username, dynamic jwt) async {
    // try {
    String graphQLDocument = '''
     query MyQuery{
    qGetUser(userName: "$username"){
      cleverlynks(limit:1000){
        items {
          cleverlynk {
            id
        ordersByDateRange(
          startDate:  `"${_formatDate(begin)}"` 
          endDate: `"$end"` 
        ){
          items{  
            id
            check
            newOrder
            deliveryInfo {
              deliveryDate
              deliveryTime
            }
            hidden
            identification
            orderStatus
            paymentMethod
            price {
              total
            }
            status
            createdAt
          }
        }
      }
    }
  }
  }
}`;
    ''';

    //   var operation = Amplify.API.query(
    //       request: GraphQLRequest<String>(
    //     document: graphQLDocument,
    //   ));

    //   var result = await operation.response;
    //   for (var err in result.errors) {
    //     print(err.message);
    //   }
    //   print(result.data);
    // } on ApiException catch (e) {
    //   print('Query failed: $e');
    // }
    //
    http.Response response;
    var body = {'OperationName': 'qGetUser', 'query': graphQLDocument};

    final directory = await getTemporaryDirectory();
    final path = directory.path + '/orders.json';
    File file = File(path);

    try {
      if (file.existsSync()) {
        file.readAsString().then((value) {
          compute(parseOrders, [value, begin]).then((res) {
            ordersToShow = res;
            notifyListeners();
          });
        });
      }

      http
          .post(
        Uri.parse(
            'https://igojikuei5b4hn2bfyremk7yde.appsync-api.us-east-1.amazonaws.com/graphql'),
        headers: {
          'Authorization': jwt,
          'Content-Type': 'application/json',
        },
        body: json.encode(body),
      )
          .then((value) {
        if (value.statusCode != 200) {
          throw HttpException("Hubo un error con la petición");
        }
        response = value;
        orders.clear();
        var cleverlynks = jsonDecode(response.body)['data']['qGetUser']
            ['cleverlynks']['items'];
        for (var c in cleverlynks) {
          var ordersArray = c['cleverlynk']['ordersByDateRange']['items'];
          if (ordersArray.length > 0) {
            for (var o in ordersArray) {
              var order = OrderLiteModel(
                  o['id'],
                  DateTime.parse(o['createdAt']),
                  o['deliveryInfo']['deliveryDate'],
                  o['status'],
                  o['orderStatus'],
                  o['price']['total'],
                  o['hidden']);
              orders.add(order);
            }
          }
        }
        if (orders.isNotEmpty) {
          orders.sort((a, b) => -a.createdAt.compareTo(b.createdAt));
          ordersToShow = orders;
          notifyListeners();
          var jsonString = jsonEncode(ordersToShow);
          file.writeAsString(jsonString);
        } else {
          ordersLastWeek = false;
          file.exists().then((value) {
            if (value) {
              file.delete();
            }
          });
          notifyListeners();
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void filterOrdersByStatus(OrderStatusFilters filterStatus) {
    switch (filterStatus) {
      case OrderStatusFilters.ACCEPTED:
        ordersToShow = orders
            .where((order) => acceptedOrderCodes.contains(order.orderStatus))
            .toList();
        break;
      case OrderStatusFilters.ARCHIVED:
        ordersToShow = orders.where((order) => order.hidden).toList();
        break;
      case OrderStatusFilters.PENDING:
        ordersToShow = orders
            .where((order) => pendingOrderCodes.contains(order.orderStatus))
            .toList();
        break;
      case OrderStatusFilters.DELIVERED:
        ordersToShow = orders.where((order) => order.status == 2).toList();
        break;
      case OrderStatusFilters.REJECTED:
        ordersToShow = orders
            .where((order) => rejectedOrderCodes.contains(order.orderStatus))
            .toList();
        break;
      case OrderStatusFilters.ALL:
        ordersToShow = [...orders];
        break;
      default:
        ordersToShow = [...orders];
        break;
    }
    if (ordersToShow.isEmpty) {
      ordersLastWeek = false;
    } else {
      ordersLastWeek = true;
    }
    notifyListeners();
  }

  void updateOrderStatus(String orderId, bool hidden, String jwt) async {
    bool updateHidden = !hidden;
    String graphqlOperation = '''mutation updateCleverlynk{
                cUpdateOrder(input: {id: "$orderId", hidden: $updateHidden}) {
                  id
                  hidden
                }
          }''';
    http.Response response;
    var body = {'OperationName': 'cUpdateoRDER', 'query': graphqlOperation};
    try {
      response = await http.post(
        Uri.parse(
            'https://igojikuei5b4hn2bfyremk7yde.appsync-api.us-east-1.amazonaws.com/graphql'),
        headers: {
          'Authorization': jwt,
          'Content-Type': 'application/json',
        },
        body: json.encode(body),
      );
      int indexInList = orders.indexWhere((element) => element.id == orderId);
      int indexInOrdersToShow =
          ordersToShow.indexWhere((element) => element.id == orderId);

      if (indexInList >= 0) {
        OrderLiteModel currentOrder = orders[indexInList];
        OrderLiteModel updateOrder =
            _updateOrderInPosition(indexInList, currentOrder, updateHidden);
        orders[indexInList] = updateOrder;
      }
      if (indexInOrdersToShow >= 0) {
        ordersToShow.removeAt(indexInOrdersToShow);
      }
    } catch (e) {
      print(e);
    }
  }

  OrderLiteModel _updateOrderInPosition(
      int indexInList, OrderLiteModel currentOrder, bool hidden) {
    return OrderLiteModel(
        currentOrder.id,
        currentOrder.createdAt,
        currentOrder.deliveryDate,
        currentOrder.status,
        currentOrder.orderStatus,
        currentOrder.total,
        hidden);
  }
}

List<OrderLiteModel> parseOrders(List<dynamic> values) {
  final value = values[0];
  final date = values[1];
  final parsed = jsonDecode(value).cast<Map<String, dynamic>>();
  List<OrderLiteModel> orders = parsed
      .map<OrderLiteModel>((json) => OrderLiteModel.fromJson(json))
      .toList();
  orders.sort((a, b) => -a.createdAt.compareTo(b.createdAt));
  final e = orders.firstWhere((element) => element.createdAt.isBefore(date));
  final end = orders.indexOf(e);
  return orders.sublist(0, end);
}

String _formatDate(DateTime date) {
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final String formatted = formatter.format(date);
  return formatted;
}
