import 'package:flutter/material.dart';
import 'dart:collection';

class AppModel extends ChangeNotifier {
  int _index = 0;

  int get index => _index;

  var _screensHistory = new Queue<int>();

  set index(int value) {
    _index = value;
    notifyListeners();
  }

  bool removeScreen() {
    if (_screensHistory.isEmpty) {
      return true;
    }
    index = _screensHistory.removeFirst();
    return false;
  }

  void addHistory(int i) {
    _screensHistory.add(i);
  }
}
