import 'package:flutter/widgets.dart';
import 'package:clever_app/models/OrderListModel.dart';

class OrderLiteModel extends ChangeNotifier {
  String realId;
  String id;
  DateTime createdAt;
  String deliveryDate;
  String stringStatus;
  double total;
  int orderStatus;
  int status;
  bool hidden;

  OrderLiteModel(String id, DateTime createdAt, String deliveryDate, int status,
      int orderStatus, double total, bool hidden) {
    this.realId = id;
    this.id = id.contains("-") ? id.split("-")[1] : id;
    this.createdAt = createdAt;
    this.deliveryDate = deliveryDate != null ? deliveryDate : "N/A";
    this.total = total;
    this.orderStatus = orderStatus;
    this.status = status;
    this.hidden = hidden;
    if (acceptedOrderCodes.contains(orderStatus)) {
      if (orderStatus == 701 && status == 0) {
        this.stringStatus = "Seleccionado";
      } else {
        switch (status) {
          case 0:
            this.stringStatus = "Confirmada";
            break;
          case 1:
            this.stringStatus = "En Preparación";
            break;
          case 2:
            this.stringStatus = "Despachada";
            break;
          default:
            this.stringStatus = "Confirmada";
            break;
        }
      }
    } else if (orderStatus == 203 || orderStatus == 201) {
      this.stringStatus = "Pendiente";
    } else if (orderStatus == 202) {
      this.stringStatus = "Rechazada";
    } else {
      switch (orderStatus % 100) {
        case 2:
        case 4:
        case 9:
        case 10:
        case 12:
          this.stringStatus = "Rechazada";
          break;
        case 11:
          this.stringStatus = "Cancelada";
          break;
        case 3:
          this.stringStatus = "Pendiente";
          break;
        case 13:
          this.stringStatus = "Revertida";
          break;
        default:
          this.stringStatus = "Rechazada";
          break;
      }
    }
  }

  OrderLiteModel.fromJson(Map<String, dynamic> json)
      : realId = json['realId'] as String,
        id = json['id'] as String,
        createdAt = DateTime.parse(json['createdAt']),
        deliveryDate = json['deliveryDate'] as String,
        stringStatus = json['stringStatus'] as String,
        total = json['total'] as double,
        orderStatus = json['orderStatus'] as int,
        status = json['status'] as int,
        hidden = json['hidden'] as bool;

  Map<String, dynamic> toJson() => {
        'realId': realId,
        'id': id,
        'createdAt': createdAt.toString(),
        'deliveryDate': deliveryDate,
        'stringStatus': stringStatus,
        'total': total,
        'orderStatus': orderStatus,
        'status': status,
        'hidden': hidden
      };
}
