import 'dart:io';

import 'package:clever_app/models/ItemForOrderModel.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class OrderDetailModel extends ChangeNotifier {
  String deliveryAddress = "";
  String deliveryAdditionalInfo = "";
  double deliveryPrice = 0;
  List<ItemForOrderModel> items = [];
  double subtotal = 0;
  String identification = "";
  double tip = 0;
  void fetchOrder(String orderId, String jwt) {
    // cXJ33qu4UE-RGD72
    String graphQLDocument = '''
    query q{
      qGetOrder(id:"$orderId"){
        price{
          delivery
          subTotal
          tip
        }
        items{
          name
          images
          price
          quantity
        }
        identification
        deliveryInfo{
          deliveryAddress
          deliveryAdditionalInfo
        }
      }
    }
    ''';

    http.Response response;
    var body = {'OperationName': 'qGetOrder', 'query': graphQLDocument};
    try {
      http
          .post(
        Uri.parse(
            'https://igojikuei5b4hn2bfyremk7yde.appsync-api.us-east-1.amazonaws.com/graphql'),
        headers: {
          'Authorization': jwt,
          'Content-Type': 'application/json',
        },
        body: json.encode(body),
      )
          .then((value) {
        if (value.statusCode != 200) {
          throw HttpException("Hubo un error con la petición");
        }
        response = value;
        var order = jsonDecode(response.body)['data']['qGetOrder'];

        if (order['price']['delivery'] != null) {
          deliveryPrice = order['price']['delivery'];
        }
        
        subtotal = order['price']['subTotal'];
        if (order['price']['tip'] != null) tip = order['price']['tip'];
        var itemsInOrder = order['items'];

        identification = order['identification'];

        if (order['deliveryInfo']['deliveryAddress'] != null) {
          
          deliveryAddress = order['deliveryInfo']['deliveryAddress'];
          if (order['deliveryInfo']['deliveryAdditionalInfo'] != null)
          
          deliveryAdditionalInfo =
              order['deliveryInfo']['deliveryAdditionalInfo'];
        }
        items.clear();
        for (var i in itemsInOrder) {
          ItemForOrderModel item = ItemForOrderModel(
              i['images'][0], i['name'], i['quantity'], i['price']);
          
          items.add(item);
        }
        notifyListeners();
      });
    } catch (e) {
      print(e);
    }
  }
}
