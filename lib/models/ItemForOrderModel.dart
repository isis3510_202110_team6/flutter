import 'package:flutter/widgets.dart';

class ItemForOrderModel extends ChangeNotifier {
  String image;
  String name;
  int quantity;
  double price;
  ItemForOrderModel(String image, String name, int quantity, double price) {
    this.image = image;
    this.name = name;
    this.quantity = quantity;
    this.price = price;
  }
}
