import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:geolocator/geolocator.dart';

class ThemeModel extends ChangeNotifier {
  static const THEME = "THEME";
  static const LIGHT = "LIGHT";
  static const DARK = "DARK";
  static const SYSTEM = "SYSTEM";
  static const AUTO = "AUTO";

  String internalTheme;

  String _theme;

  String get theme => _theme;

  var sunriseHour = TimeOfDay(hour: 6, minute: 0);
  var sunsetHour = TimeOfDay(hour: 18, minute: 0);

  ThemeModel() {
    _getTheme();
  }

  _setTheme(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(THEME, value);
  }

  _getTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _theme = prefs.getString(THEME);
    if (_theme != AUTO) {
      internalTheme = _theme;
      notifyListeners();
    } else {
      _setAutoTheme();
    }
  }

  _getSunriseSunset() {
    _determinePosition().then((pos) {
      var lat = pos.latitude;
      var lng = pos.longitude;
      http.get(
        Uri.parse(
            'https://api.sunrise-sunset.org/json?lat=$lat&lng=$lng&formatted=0'),
        headers: {
          'Content-Type': 'application/json',
        },
      ).then((res) {
        var response = jsonDecode(res.body);
        if (response["status"] == "OK") {
          var sunrise =
              DateTime.parse(response["results"]["sunrise"]).toLocal();
          var sunset = DateTime.parse(response["results"]["sunset"]).toLocal();
          sunriseHour = TimeOfDay.fromDateTime(sunrise);
          sunsetHour = TimeOfDay.fromDateTime(sunset);
          _setAutoTheme();
        } else {
          _setAutoTheme();
        }
      }).catchError((e) {
        print(e);
        _setAutoTheme();
      });
    }).catchError((e) {
      print(e);
      _setAutoTheme();
    });
  }

  _setAutoTheme() {
    var now = DateTime.now();
    var sunrise = DateTime(
        now.year, now.month, now.day, sunriseHour.hour, sunriseHour.minute);
    var sunset = DateTime(
        now.year, now.month, now.day, sunsetHour.hour, sunsetHour.minute);
    int timerSeconds;

    if (now.isAfter(sunrise) && now.isBefore(sunset)) {
      internalTheme = LIGHT;
      timerSeconds = sunset.difference(now).inMilliseconds;
    } else if (now.isAfter(sunset)) {
      internalTheme = DARK;
      DateTime nextDayMorningTime = sunrise.add(Duration(days: 1));
      timerSeconds = nextDayMorningTime.difference(now).inMilliseconds;
    } else {
      internalTheme = DARK;
      timerSeconds = sunrise.difference(now).inMilliseconds;
    }

    notifyListeners();

    Timer(
      Duration(milliseconds: timerSeconds),
      () {
        _setAutoTheme();
      },
    );
  }

  set theme(String value) {
    if (value == AUTO) {
      _getSunriseSunset();
    }
    internalTheme = value;
    _theme = value;
    _setTheme(value);
    notifyListeners();
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.lowest,
        timeLimit: Duration(seconds: 3));
  }
}
