import 'package:clever_app/models/CleverlynkLiteModel.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

class CleverlynkListModel extends ChangeNotifier {
  List<CleverlynkLiteModel> cleverlynksList = [];
  bool noClynks = false;

  void fetchCleverlynks(String username, String jwt) async {
    String graphQLDocument = '''
     query getCleverlynks{
        qGetUser(userName: "$username"){
          cleverlynks(limit:1000){
            items{
              cleverlynk{
                id
                name
                newOrdersAmount
                amountRenders
                status
              }
            }
          }
        }
      }
    ''';
    http.Response response;
    var body = {'OperationName': 'qGetUser', 'query': graphQLDocument};

    final directory = await getTemporaryDirectory();
    final path = directory.path + '/clynks.json';
    File file = File(path);

    try {
      if (file.existsSync()) {
        file.readAsString().then((value) {
          compute(parseClynks, value).then((val) {
            cleverlynksList = val;
            notifyListeners();
          });
        });
      }

      http
          .post(
        Uri.parse(
            'https://igojikuei5b4hn2bfyremk7yde.appsync-api.us-east-1.amazonaws.com/graphql'),
        headers: {
          'Authorization': jwt,
          'Content-Type': 'application/json',
        },
        body: json.encode(body),
      )
          .then((value) {
        if (value.statusCode != 200) {
          throw HttpException("Hubo un error con la petición");
        }
        response = value;
        cleverlynksList.clear();
        var cleverlynks = jsonDecode(response.body)['data']['qGetUser']
            ['cleverlynks']['items'];
        for (var cl in cleverlynks) {
          var c = cl['cleverlynk'];
          var cleverlynk = CleverlynkLiteModel(
              c['id'],
              c['name'],
              c['status'] == "START",
              c['newOrdersAmount'],
              c['amountRenders'],
              c['status']);
          cleverlynksList.add(cleverlynk);
        }
        if (cleverlynksList.isNotEmpty) {
          notifyListeners();
          var jsonString = jsonEncode(cleverlynksList);
          file.writeAsString(jsonString);
        } else {
          noClynks = true;
          file.exists().then((value) {
            if (value) {
              file.delete();
            }
          });
          notifyListeners();
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void updateCleverlynkStatus(
      String cleverlynkId, bool isActive, String jwt) async {
    var status = isActive ? "DISABLED" : "START";
    String graphqlOperation = '''mutation updateCleverlynk{
              mUpdateCleverlynk(input: {id: "$cleverlynkId", status: $status}) {
                id
                status
              }
        }''';
    http.Response response;
    var body = {
      'OperationName': 'mUpdateCleverlynk',
      'query': graphqlOperation
    };
    try {
      response = await http.post(
        Uri.parse(
            'https://igojikuei5b4hn2bfyremk7yde.appsync-api.us-east-1.amazonaws.com/graphql'),
        headers: {
          'Authorization': jwt,
          'Content-Type': 'application/json',
        },
        body: json.encode(body),
      );
      

      var indexInList =
          cleverlynksList.indexWhere((element) => element.id == cleverlynkId);
      if (indexInList >= 0) {
        var currentCleverlynk = cleverlynksList[indexInList];
        var updatedCleverlynk =
            _updateCleverlynkInPosition(currentCleverlynk, isActive, status);
        cleverlynksList[indexInList] = updatedCleverlynk;
        notifyListeners();
      
      }
    } catch (e) {
      print(e);
    }
  }

  CleverlynkLiteModel _updateCleverlynkInPosition(
      CleverlynkLiteModel currentCleverlynk, bool isActive, String status) {
    return CleverlynkLiteModel(
        currentCleverlynk.id,
        currentCleverlynk.name,
        !currentCleverlynk.isActive,
        currentCleverlynk.newOrdersAmount,
        currentCleverlynk.amountRenders,
        status);
  }
}

List<CleverlynkLiteModel> parseClynks(String value) {
  final parsed = jsonDecode(value).cast<Map<String, dynamic>>();
  return parsed
      .map<CleverlynkLiteModel>((json) => CleverlynkLiteModel.fromJson(json))
      .toList();
}
