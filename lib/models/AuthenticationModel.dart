import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:flutter/widgets.dart';
import 'package:amazon_cognito_identity_dart_2/cognito.dart';

enum Status { Uninitialized, Authenticated, Authenticating, Unauthenticated }

class AuthenticationModel extends ChangeNotifier {
  String _username;
  Status _status = Status.Uninitialized;
  final _userPool = new CognitoUserPool(
    'us-east-1_sabc1AhJC',
    '735immiv1v8knj418te27tco04',
  );
  CognitoUser _cognitoUser;
  CognitoUserSession _session;

  var _jwt;

  Status get status => _status;
  String get username => _username;
  dynamic get jwt => _jwt;

  AuthenticationModel() {
    fetchSession();
  }

  void fetchSession() async {
    try {
      _status = Status.Authenticating;
      CognitoAuthSession res = await Amplify.Auth.fetchAuthSession(
          options: CognitoSessionOptions(getAWSCredentials: true));
      if (res.isSignedIn) {
        AuthUser user = await Amplify.Auth.getCurrentUser();
        _username = user.username;
        _cognitoUser = new CognitoUser(_username, _userPool);
        _session = _cognitoUser.getCognitoUserSession({
          'IdToken': res.userPoolTokens.idToken,
          'AccessToken': res.userPoolTokens.accessToken,
          'RefreshToken': res.userPoolTokens.refreshToken
        });
        _jwt = _session.getIdToken().getJwtToken();
        _status = Status.Authenticated;
      } else {
        _status = Status.Unauthenticated;
      }
    } on AuthException catch (e) {
      _status = Status.Unauthenticated;
      print(e.message);
      throw e;
    } on AmplifyException catch (e) {
      _status = Status.Unauthenticated;
      print(e.message);
      throw e;
    }
    notifyListeners();
  }

  Future<bool> signInWithUsernameAndPassword(
      String username, String password) async {
    try {
      _status = Status.Authenticating;
      var signIn = await Amplify.Auth.signIn(
        username: username,
        password: password,
      );
      if (signIn.isSignedIn) {
        var authDetails = new AuthenticationDetails(
          username: username,
          password: password,
        );
        _cognitoUser = new CognitoUser(username, _userPool);
        try {
          _session = await _cognitoUser.authenticateUser(authDetails);
          _jwt = _session.getIdToken().getJwtToken();
        } catch (e) {
          print(e);
        }
        this._username = username;
        _status = Status.Authenticated;
        return true;
      } else {
        _status = Status.Unauthenticated;
        return false;
      }
    } on AuthException {
      _status = Status.Unauthenticated;
      return false;
    }
  }

  void signOut() async {
    try {
      await Amplify.Auth.signOut();
      await _cognitoUser.signOut();
      _jwt = null;
      _status = Status.Unauthenticated;
    } on AuthException catch (e) {
      print(e.message);
    }
    notifyListeners();
  }
}
