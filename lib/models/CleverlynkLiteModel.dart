import 'package:flutter/widgets.dart';

class CleverlynkLiteModel extends ChangeNotifier {
  String id;
  String name;
  bool isActive;
  int newOrdersAmount;
  int amountRenders;
  String status;

  CleverlynkLiteModel(String id, String name, bool active, int newOrdersAmount,
      int amountRenders, String status) {
    this.id = id;
    this.name = name;
    this.isActive = active;
    this.newOrdersAmount = newOrdersAmount;
    this.amountRenders = amountRenders;
    this.status = status;
  }

  CleverlynkLiteModel.fromJson(Map<String, dynamic> json)
      : id = json['id'] as String,
        name = json['name'] as String,
        isActive = json['isActive'] as bool,
        newOrdersAmount = json['newOrdersAmount'] as int,
        amountRenders = json['amountRenders'] as int,
        status = json['status'] as String;

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'isActive': isActive,
        'newOrdersAmount': newOrdersAmount,
        'amountRenders': amountRenders,
        'status': status
      };
}
